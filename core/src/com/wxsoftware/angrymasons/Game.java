package com.wxsoftware.angrymasons;

import com.badlogic.gdx.Screen;
import com.wxsoftware.angrymasons.view.World;
import com.wxsoftware.angrymasons.view.WorldRenderer;

/**
 * Created by alberto on 18/01/18.
 */

public class Game implements Screen {

    AngryMasons game;
    World world;
    WorldRenderer render;

    public Game (AngryMasons game){
        this.game=game;
        world=new World(game);
        render=new WorldRenderer(world);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        world.update();
        render.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        world.dispose();
    }
}
