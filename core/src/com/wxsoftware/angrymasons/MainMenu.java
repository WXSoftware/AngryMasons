package com.wxsoftware.angrymasons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;

import javax.swing.GroupLayout;


/**
 * Created by alberto on 17/01/18.
 * Clase que implementa un menú de elección
 */

public class MainMenu implements Screen{

    private AngryMasons game;
    Stage stage;
    BitmapFont black;
    BitmapFont white;
    TextureAtlas atlas;
    //contenedor para textureAtlas, json, etc.
    Skin skin;
    SpriteBatch batch;
    TextButton button;

    //etiqueta para mostrar texto
    Label label;

    //variable para la internalización
    Traductor traductor;

    public MainMenu (AngryMasons game){
        this.game=game;

    }



    @Override
    public void show() {
        batch=new SpriteBatch();
        atlas=new TextureAtlas("button.pack");
        skin=new Skin();
        skin.addRegions(atlas);
        black=new BitmapFont(Gdx.files.internal("font.fnt"), false);
        white=new BitmapFont(Gdx.files.internal("whitefont.fnt"), false);

        //inicialiamos el traductor
        traductor=new Traductor();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //dibujamos la stage
        stage.act(delta);
        batch.begin();
        stage.draw();
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        if (stage==null){
            stage=new Stage();
            stage.getViewport().update(width, height, true);
        }
        stage.clear();

        //captura las interacciones con el jugador.
        Gdx.input.setInputProcessor(stage);

        //establecemos el estilo de los botones cuando están arriba
        //o cuando están presionados.
        TextButton.TextButtonStyle style=new TextButton.TextButtonStyle();
        style.up=skin.getDrawable("buttonnormal");
        style.down=skin.getDrawable("buttonpressed");
        style.font=black;

        //tenemos que añadirle al botón el texto en función del idioma.
        button=new TextButton(traductor.getString("play"), style);
        button.getLabel().setFontScale(2.5f);
        button.setWidth(800);
        button.setHeight(200);
        button.setX(Gdx.graphics.getWidth()/2 - button.getWidth()/2);
        button.setY(Gdx.graphics.getHeight()/2 - button.getHeight()/2);

        //le añadimos el listener al botón
        button.addListener(new InputListener(){
            public void touchUp(InputEvent event, float x, float y, int pointer, int button){
                game.setScreen(new Game(game));
            }
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                return true;
            }
        });


        //vamos a añadir una etiqueta
        Label.LabelStyle ls=new Label.LabelStyle(white, Color.WHITE);
        label=new Label("Angry Masons", ls);
        label.setFontScale(3f);
        label.setX(0);
        label.setY(Gdx.graphics.getHeight()/2+150);
        label.setWidth(width);
        label.setAlignment(Align.center);


        //añadimos el actor boton y la etiqueta
        stage.addActor(button);
        stage.addActor(label);




    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        skin.dispose();
        atlas.dispose();
        white.dispose();
        black.dispose();
        stage.dispose();
    }
}
