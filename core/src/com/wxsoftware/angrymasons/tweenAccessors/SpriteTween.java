package com.wxsoftware.angrymasons.tweenAccessors;

import com.badlogic.gdx.graphics.g2d.Sprite;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * Created by alberto on 15/01/18.
 */

/**
 * Clase para animar la splashscreen para que pase de transparente a opaca.
 */
public class SpriteTween implements TweenAccessor<Sprite> {

    public static final int ALPHA=1;

    @Override
    public int getValues(Sprite target, int tweenType, float[] returnValues) {
        switch(tweenType){
            case ALPHA:
                returnValues[0]=target.getColor().a;
                //Devolvemos la cantidad de datos de returnValues que queremos obtener
                return 1;
            default:
                return 0;
        }
    }

    @Override
    public void setValues(Sprite target, int tweenType, float[] newValues) {
        switch (tweenType){
            case ALPHA:
                target.setColor(1, 1, 1, newValues[0]);
                break;

        }
    }
}
