package com.wxsoftware.angrymasons.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wxsoftware.angrymasons.AngryMasons;
import com.wxsoftware.angrymasons.MainMenu;
import com.wxsoftware.angrymasons.tweenAccessors.SpriteTween;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

/**
 * Created by alberto on 15/01/18.
 */

public class SplashScreen implements Screen {

    Texture splashTexture;
    Sprite splashSprite;
    SpriteBatch batch;

    AngryMasons game;

    TweenManager manager;

    public SplashScreen(AngryMasons game){
        this.game=game;
    }

    @Override
    public void show() {

        //las fotos las insertamos como texturas
        splashTexture=new Texture("splashscreen.png");
        //le insertamos un filtro para cuando se redimensione
        splashTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        //debemos dibujar un sprite, al que se le asigna una textura.
        splashSprite=new Sprite(splashTexture);
        //le damos el color inicial
        splashSprite.setColor(1,1,1,0);
        //se le da un punto de origen para que se redimensione
        splashSprite.setOrigin(splashSprite.getWidth()/2f, splashSprite.getHeight()/2f);
        //establecemos su punto de origen. el o,o se establece en la esquina inferior
        //izquierda
        splashSprite.setPosition((Gdx.graphics.getWidth()/2)-(splashSprite.getWidth()/2), (Gdx.graphics.getHeight()/2)-(splashSprite.getHeight()/2));

        batch=new SpriteBatch();

        //esto es para animar la splashscreen. El primer parámetro es el objeto del que está formado
        //el TweenAccessors<Sprite>. El segundo un SpriteTween nuevo
        Tween.registerAccessor(Sprite.class, new SpriteTween());

        manager=new TweenManager();
        //animamos el splashSprite para que pase de transparente a opaco

        //tenemos que implementar un callback para salir de esta screen
        TweenCallback cb=new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                tweenCompleted();
            }
        };

        //.to interpola desde el valor actual hasta el objetivo target
        Tween.to(splashSprite, SpriteTween.ALPHA, 3.5f)
                .target(1)
                .ease(TweenEquations.easeInQuad)
                .repeatYoyo(1, 2.5f)
                .setCallback(cb)
                .setCallbackTriggers(TweenCallback.COMPLETE)
                .start(manager);
    }


    private void tweenCompleted(){
        game.setScreen(new MainMenu(game));
    }


    @Override
    public void render(float delta) {
        //Limpiamos la pantalla con el color negro
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //actualizamos el manager del tween
        manager.update(delta);

        //inciamos el batch
        batch.begin();
        //dibujamos el batch
        splashSprite.draw(batch);
        //finalizamos el batch
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
