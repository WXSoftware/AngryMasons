package com.wxsoftware.angrymasons;

import com.wxsoftware.angrymasons.traductor.MakeTranslation;
import com.wxsoftware.angrymasons.traductor.defaultLanguage;
import com.wxsoftware.angrymasons.traductor.spanishLanguage;


/**
 * Created by alberto on 18/01/18.
 */

public class Traductor {

    String terminalLanguage;
    MakeTranslation makeTranslation;

    public Traductor(){
       terminalLanguage=java.util.Locale.getDefault().toString();
       String subTerminalLanguage=terminalLanguage.substring(0,2);

       if (subTerminalLanguage.equalsIgnoreCase("ES")){
           makeTranslation=new MakeTranslation(new spanishLanguage());
       }else makeTranslation=new MakeTranslation(new defaultLanguage());
    }


    public String getString(String tag){
        return makeTranslation.employLanguageStrategy(tag);
    }
}
