package com.wxsoftware.angrymasons.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by alberto on 19/01/18.
 */

public interface Entity {

    Vector2 getPosition();
    float getWidth();
    float getHeight();
    Rectangle getBounds();
    float getSpeed();
    float getRotation();
    void setPosition(Vector2 position);
    void setWidth(float width);
    void setHeight(float height);
    void setBounds(Rectangle bounds);
    void setSpeed(float speed);
    void setRotation(float rotation);

}


