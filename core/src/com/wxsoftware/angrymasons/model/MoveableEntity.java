package com.wxsoftware.angrymasons.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class MoveableEntity implements Entity{

    protected Vector2 position;
    protected float width;
    protected float height;
    protected Rectangle bounds;
    protected float speed;
    protected float rotation;


    @Override
    public abstract Vector2 getPosition();

    @Override
    public abstract float getWidth();

    @Override
    public abstract float getHeight();

    @Override
    public abstract Rectangle getBounds();

    @Override
    public abstract float getSpeed();

    @Override
    public abstract float getRotation();

    @Override
    public abstract void setPosition(Vector2 position);

    @Override
    public abstract void setWidth(float width);

    @Override
    public abstract void setHeight(float height);

    @Override
    public abstract void setBounds(Rectangle bounds);

    @Override
    public abstract void setSpeed(float speed);

    @Override
    public abstract void setRotation(float rotation);

    public abstract void update();

}
