package com.wxsoftware.angrymasons.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by alberto on 19/01/18.
 */

public class Ship extends MoveableEntity {


    public Ship(){

    }

    public Ship(float speed, float rotation, float width,
                 float height, Vector2 position){
        setSpeed(speed);
        setRotation(rotation);
        setWidth(width);
        setHeight(height);
        setPosition(position);
        this.bounds=new Rectangle(position.x, position.y,
                width,height);

    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public float getWidth() {
        return width;
    }

    @Override
    public float getHeight() {
        return height;
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public float getSpeed() {
        return speed;
    }

    @Override
    public float getRotation() {
        return rotation;
    }

    @Override
    public void setPosition(Vector2 position) {
        this.position=position;
    }

    @Override
    public void setWidth(float width) {
        this.width=width;
    }

    @Override
    public void setHeight(float height) {
        this.height=height;
    }

    @Override
    public void setBounds(Rectangle bounds) {
        this.bounds=bounds;
    }

    @Override
    public void setSpeed(float speed) {
        this.speed=speed;
    }

    @Override
    public void setRotation(float rotation) {
        this.rotation=rotation;
    }

    @Override
    public void update() {

    }
}
