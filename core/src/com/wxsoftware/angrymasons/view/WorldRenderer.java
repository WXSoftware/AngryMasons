package com.wxsoftware.angrymasons.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wxsoftware.angrymasons.model.Ship;

/**
 * Created by alberto on 19/01/18.
 */

public class WorldRenderer {

    World world;
    SpriteBatch batch;
    Ship ship;
    Camera cam;
    Texture shipTexture;

    public WorldRenderer(World world){
         this.world=world;
         batch=new SpriteBatch();
         cam=new OrthographicCamera();
         shipTexture=new Texture("ship.png");
    }


    public void render(){
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        ship=world.getShip();
        batch.begin();
        batch.draw(shipTexture, ship.getPosition().x, ship.getPosition().y);
        batch.end();

    }

    public void dispose(){
        batch.dispose();
        shipTexture.dispose();
    }
}
