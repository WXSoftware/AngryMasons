package com.wxsoftware.angrymasons.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.wxsoftware.angrymasons.AngryMasons;
import com.wxsoftware.angrymasons.model.Ship;

/**
 * Created by alberto on 19/01/18.
 */

public class World {

    private AngryMasons game;
    Ship ship;

    public World(AngryMasons game){
        this.game=game;
        ship=new Ship(5f,0,1,1,
                new Vector2(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2));
    }

    public Ship getShip(){
        return this.ship;
    }

    public void update(){
        ship.update();
    }


    public void dispose(){

    }
}
