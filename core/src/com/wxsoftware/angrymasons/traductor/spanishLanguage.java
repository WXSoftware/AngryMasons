package com.wxsoftware.angrymasons.traductor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

public class spanishLanguage implements  LanguageStrategy{
    FileHandle baseFileHandle;
    Locale locale;
    I18NBundle language;

    public spanishLanguage() {
        baseFileHandle= Gdx.files.internal("strings");
        locale=new Locale("es");
        language=I18NBundle.createBundle(baseFileHandle, locale);
    }

    @Override
    public String traducedString(String tag) {

        return language.get(tag);
    }
}
