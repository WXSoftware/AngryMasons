package com.wxsoftware.angrymasons.traductor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

public class defaultLanguage implements LanguageStrategy {
    FileHandle baseFileHandle;
    I18NBundle language;

    public defaultLanguage(){
    baseFileHandle= Gdx.files.internal("strings");
    language=I18NBundle.createBundle(baseFileHandle, Locale.ROOT);
    }

    @Override
    public String traducedString(String tag) {

        return language.get(tag);
    }
}
