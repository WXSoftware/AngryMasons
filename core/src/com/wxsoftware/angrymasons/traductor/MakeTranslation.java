package com.wxsoftware.angrymasons.traductor;

/**
 * Created by alberto on 19/01/18.
 */

public class MakeTranslation {
    private LanguageStrategy languageStrategy;

    public MakeTranslation (LanguageStrategy strategy){
        this.languageStrategy=strategy;
    }

    public String employLanguageStrategy(String tag){
        return this.languageStrategy.traducedString(tag);
    }
}
